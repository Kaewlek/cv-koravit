const { createSlice } = require('@reduxjs/toolkit');

const cartSlice = createSlice({
  name: 'cart',
  initialState: {
    data: [],
    isOpenCart: false,
  },
  reducers: {
    addProduct: (state, action) => {
      const isSelected = state.data.find((f) => f.id === action.payload.id);
      let result = [];
      if (isSelected) {
        result = state.data.map((item) => {
          return item.id === action.payload.id
            ? { ...item, count: Number(item.count) + 1 }
            : item;
        });
      } else {
        result = [...state.data, { ...action.payload, count: 1 }];
      }
      return {
        ...state,
        data: result,
      };
    },
    addProductFromInput: (state, action) => {
      let result = Number(action.payload.count);
      if (action.payload.count < 1) {
        result = 1;
      }
      return {
        ...state,
        data: state.data.map((item) => {
          return item.id === action.payload.id
            ? { ...item, count: result }
            : item;
        }),
      };
    },
    deleteProduct: (state, action) => {
      return {
        ...state,
        data: [...state.data.filter((item) => item.id !== action.payload.id)],
      };
    },
    handleClickCart: (state) => {
      return {
        ...state,
        isOpenCart: !state.isOpenCart,
      };
    },
  },
});

export const {
  addProduct,
  deleteProduct,
  handleClickCart,
  addProductFromInput,
} = cartSlice.actions;

export const addProductAsync = (payload) => (dispatch) => {
  dispatch(addProduct(payload));
};

export const addProductFromInputAsync = (payload) => (dispatch) => {
  dispatch(addProductFromInput(payload));
};

export const deleteProductAsync = (payload) => (dispatch) => {
  dispatch(deleteProduct(payload));
};

export default cartSlice.reducer;
