export const gits = [
  {
    name: 'Gitlab',
    url: 'https://gitlab.com/Kaewlek',
  },
];
export const educations = [
  {
    time: '2009 - 2015',
    title: 'โรงเรียนบดินทรเดชา (สิงห์ สิงหเสนี) สมุทรปราการ',
    content: `ระดับการศึกษามัธยมศึกษาตอนต้น-ปลาย (วิทย์-คณิต) เกรดเฉลี่ยสะสม 3.03`,
    imgSrc: 'BDS-logo.png',
  },
  {
    time: '2015 - 2019',
    title: 'มหาวิทยาลัยบูรพา วิทยาเขตบางแสน',
    content: `ระดับการศึกษาปริญญาตรี คณะวิทยาการสารสนเทศ สาขาวิทยาการคอมพิวเตอร์ เกรดเฉลี่ยสะสม 2.38`,
    imgSrc: 'Buu-logo.png',
  },
];
export const works = [
  {
    time: '2019 - 2020',
    title: 'บริษัท โกรท มอร์ คอร์ปอเรชั่น จำกัด',
    content: `ตำแหน่ง โปรแกรมเมอร์ ใช้ภาษา C# .net core ในการพัฒนาเป็นหลัก ดูแลในส่วนของ Frontend (C# .net core mvc, jquery) Backend (C# .net core api) และ Database (Sql server)`,
  },
];
export const frontends = [
  {
    label: 'Html',
  },
  {
    label: 'Css',
  },
  {
    label: 'Javascript',
  },
  {
    label: 'ReactJS hook',
  },
  {
    label: 'Redux',
  },
  {
    label: 'Material-UI',
  },
  {
    label: 'Bootstrap',
  },
  {
    label: 'Styled components',
  },
  {
    label: 'C# .net core mvc',
  },
];
export const backends = [
  {
    label: 'Restful api',
  },
  {
    label: 'C# .net core',
  },
  {
    label: 'Entity framework',
  },
  {
    label: 'NodeJS',
  },
  {
    label: 'Express',
  },
  {
    label: 'Mongoose',
  },
  {
    label: 'Jquery',
  },
];
export const databases = [
  {
    label: 'Sql server',
  },
  {
    label: 'Stored procedure',
  },
  {
    label: 'MongoDB',
  },
];
export const others = [
  {
    label: 'Gitlab',
  },
  {
    label: 'Github',
  },
  {
    label: 'Sourcetree',
  },
];
export const positions = [
  {
    label: 'ReactJS hook',
  },
  {
    label: 'NodeJS',
  },
  {
    label: 'Full stack developer',
  },
];
