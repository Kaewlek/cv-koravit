export const products =[
  {
    id: 1,
    logo: `https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTNoD9oT_VnEYNKKeOor8U4qK5T1LF4bC2iRDD75fQdveQMHTUA`,
    name: `Nike Air Zoom Alphafly Next%`,
    brand: `Nike`,
    image: `https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/bcb01349-d30f-407b-a503-4c0e56e39922/%E0%B8%A3%E0%B8%AD%E0%B8%87%E0%B9%80%E0%B8%97%E0%B9%89%E0%B8%B2%E0%B8%A7%E0%B8%B4%E0%B9%88%E0%B8%87%E0%B9%81%E0%B8%82%E0%B9%88%E0%B8%87%E0%B8%9C%E0%B8%B9%E0%B9%89-air-zoom-alphafly-next-13jzhr.jpg`,
    detail: `เตรียมพร้อมสำหรับสถิติครั้งถัดไปที่จะดีที่สุดด้วย Nike Air Zoom Alphafly Next% ที่มาพร้อมกับโฟมตอบสนองได้ดีและส่วน Zoom Air 2 ส่วนซึ่งผสานเข้าด้วยกันเพื่อผลักดันฟอร์มการวิ่งของคุณในครั้งต่อไปให้นำหน้ากว่าใครไม่ว่าจะวิ่งมาราธอนหรือแข่งวิ่งบนท้องถนน`,
    price: 9400,
  },
  {
    id: 2,
    logo: `https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTNoD9oT_VnEYNKKeOor8U4qK5T1LF4bC2iRDD75fQdveQMHTUA`,
    name: `Nike ACG Deschutz Mt. Fuji`,
    brand: `Nike`,
    image: `https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/4384f55a-7e04-482a-b7f3-2f7623ea54b0/%E0%B8%A3%E0%B8%AD%E0%B8%87%E0%B9%80%E0%B8%97%E0%B9%89%E0%B8%B2%E0%B9%81%E0%B8%95%E0%B8%B0-acg-deschutz-mt-fuji-4j9xKx.jpg`,
    detail: `Nike ACG Deschutz กลับมาด้วยดีไซน์ที่พร้อมลุยกลางแจ้ง โดยใช้ส่วนบนที่แห้งเร็วและมีระบบลดแรงกระแทกแบบนุ่มพิเศษในจุดที่คุณต้องการ เพื่อให้เกิดเป็นความสบายในแบบรองเท้าแตะ แต่แสดงประสิทธิภาพในแบบรองเท้าผ้าใบ ส่วนกราฟิกทั่วตัวก็ยกย่องให้กับความยิ่งใหญ่ที่น่าประทับใจของภูเขาฟูจิ`,
    price: 2900,
  },
  {
    id: 3,
    logo: `https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTNoD9oT_VnEYNKKeOor8U4qK5T1LF4bC2iRDD75fQdveQMHTUA`,
    name: `Nike React Infinity Run Flyknit “Fast City”`,
    brand: `Nike`,
    image: `https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/a3a91a94-c2e9-4022-9e61-9c02417095ae/%E0%B8%A3%E0%B8%AD%E0%B8%87%E0%B9%80%E0%B8%97%E0%B9%89%E0%B8%B2%E0%B8%A7%E0%B8%B4%E0%B9%88%E0%B8%87%E0%B8%9C%E0%B8%B9%E0%B9%89-react-infinity-run-flyknit-fast-city-0Pcfqk.jpg`,
    detail: `Nike React Infinity Run Flyknit “Fast City” ดีไซน์มาเพื่อช่วยให้คุณวิ่งได้อย่างต่อเนื่อง มาพร้อมโฟมที่มากขึ้นและรายละเอียดส่วนบนที่พัฒนาขึ้น เพื่อให้สัมผัสที่แน่นกระชับและลดแรงกระแทกได้ดี ทั้งยังมีสีสันสดใสและรายละเอียดที่ได้รับอิทธิพลจากบรรยากาศของเมืองในเวลากลางคืน ผูกเชือกแล้วสัมผัสถึงศักยภาพของคู่นี้เมื่อออกลุยบนถนนได้เลย`,
    price: 6100,
  },
];
