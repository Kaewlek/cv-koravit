import React, { createContext, useState } from 'react';
import { useSnackbar } from 'notistack';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import * as yup from 'yup';
import { v4 as uuid } from 'uuid';
import { ContextDevTool } from 'react-context-devtool';

export const AppContext = createContext({});

export const AppContextProvider = ({ children }) => {
  const theme = useTheme();
  const [crud, setCrud] = useState([
    {
      id: uuid(),
      firstName: 'Koravit',
      lastName: 'Kaewlek',
      birthday: Date(),
    },
  ]);
  const [formEditCrud, setFormEditCrud] = useState({});
  const [modalCrud, setModalCrud] = useState(false);
  const lg = useMediaQuery(theme.breakpoints.up('lg'));
  const md = useMediaQuery(theme.breakpoints.up('md'));
  const sm = useMediaQuery(theme.breakpoints.up('sm'));
  const xs = useMediaQuery(theme.breakpoints.up('xs'));
  const { enqueueSnackbar } = useSnackbar();
  const schemaFormCrud = yup.object().shape({
    firstName: yup.string().required('กรุณากรอกชื่อ'),
    lastName: yup.string().required('กรุณากรอกนามสกุล'),
    birthday: yup.date().required('กรุณากรอกวันเกิด').nullable(),
  });
  const onShowNotification = (message, variant) => {
    enqueueSnackbar(message, {
      variant,
      autoHideDuration: 2000,
      anchorOrigin: {
        vertical: md ? 'bottom' : 'top',
        horizontal: md ? 'left' : 'center',
      },
    });
  };
  const store = {
    notification: onShowNotification,
    breakpoints: { xs, sm, md, lg },
    schemaFormCrud,
    crud: {
      data: [crud, setCrud],
      edit: [formEditCrud, setFormEditCrud],
      modal: [modalCrud, setModalCrud],
    },
  };
  return (
    <AppContext.Provider value={store}>
      {children}
      <ContextDevTool context={AppContext} id="AppContext" displayName="App" />
    </AppContext.Provider>
  );
};
