import React, { useState, createContext } from 'react';

export const HeaderContext = createContext({});

export const HeaderContextProvider = ({ children }) => {
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);
  const [menuSelected, setMenuSelected] = useState(-1);
  const store = {
    mobile: [mobileMoreAnchorEl, setMobileMoreAnchorEl],
    menuSelected: [menuSelected, setMenuSelected],
  };

  return (
    <HeaderContext.Provider value={store}>{children}</HeaderContext.Provider>
  );
};
