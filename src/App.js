import React from 'react';
import {
  StylesProvider,
  CssBaseline,
  createMuiTheme,
  ThemeProvider,
  IconButton,
} from '@material-ui/core';
import Header from './Layout/Header';
import Content from './Layout/Content';
import { BrowserRouter as Router } from 'react-router-dom';
import { AppContextProvider } from './Contexts/app.store';
import { HeaderContextProvider } from './Contexts/header.store';
import { SnackbarProvider } from 'notistack';
import { MuiPickersUtilsProvider } from 'material-ui-thai-datepickers';
import MomentUtils from '@date-io/moment';
import moment from 'moment';
import 'moment/locale/th';
import Routes from './Routes';
import CloseIcon from '@material-ui/icons/Close';
const theme = createMuiTheme({
  typography: {
    fontFamily: ['Kanit', 'sans-serif'].join(','),
  },
});
function App() {
  const notistackRef = React.createRef();
  const onClickDismiss = (key) => () => {
    notistackRef.current.closeSnackbar(key);
  };
  return (
    <Router>
      <StylesProvider injectFirst>
        <ThemeProvider theme={theme}>
          <MuiPickersUtilsProvider
            libInstance={moment}
            locale="th"
            utils={MomentUtils}
          >
            <SnackbarProvider
              ref={notistackRef}
              action={(key) => (
                <IconButton size="small" onClick={onClickDismiss(key)}>
                  <CloseIcon htmlColor="white" />
                </IconButton>
              )}
            >
              <AppContextProvider>
                <CssBaseline />
                <HeaderContextProvider>
                  <Header />
                </HeaderContextProvider>
                <Content>
                  <Routes />
                </Content>
              </AppContextProvider>
            </SnackbarProvider>
          </MuiPickersUtilsProvider>
        </ThemeProvider>
      </StylesProvider>
    </Router>
  );
}

export default App;
