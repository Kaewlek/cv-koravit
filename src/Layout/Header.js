import React, { useState, Fragment } from 'react';
import { makeStyles, Button, ListItemIcon } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MobileHeader from './MobileHeader';
import { HeaderContext } from '../Contexts/header.store';
import { useContext } from 'react';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { routes } from '../Routes';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import PersonIcon from '@material-ui/icons/Person';
import MenuBookIcon from '@material-ui/icons/MenuBook';
const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    marginLeft: theme.spacing(2),
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}));
export default function Header() {
  const classes = useStyles();
  const history = useHistory();
  const { mobile, menuSelected } = useContext(HeaderContext);
  const [anchorEl, setAnchorEl] = useState(null);

  const handleMobileMenuOpen = (event) => {
    mobile[1](event.currentTarget);
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (event) => {
    setAnchorEl(null);
  };
  const handleMenuSelect = (event, index) => {
    menuSelected[1](index);
    setAnchorEl(null);
  };
  return (
    <div className={classes.grow}>
      <AppBar color="inherit">
        <Toolbar>
          <Fragment>
            <Button
              onClick={() => {
                history.goBack();
              }}
              startIcon={<ArrowBackIcon />}
              disabled={history.length === 0}
            >
              กลับ
            </Button>
            {/* <Avatar src="profile.jpeg"></Avatar>
            <Typography className={classes.title} variant="h6" noWrap>
              กรวิชญ์ แก้วเล็ก
            </Typography> */}
          </Fragment>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            <>
              <Button
                size="large"
                onClick={handleClick}
                endIcon={<ExpandMoreIcon />}
                startIcon={<MenuBookIcon />}
              >
                สิ่งที่ทำได้
              </Button>
              <Menu
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
              >
                {routes
                  .filter(({ isShow }) => {
                    return isShow;
                  })
                  .map((item, index) => (
                    <MenuItem
                      key={index}
                      component={RouterLink}
                      to={item.path}
                      onClick={(event) => handleMenuSelect(event, index)}
                      selected={index === menuSelected[0]}
                    >
                      <ListItemIcon>{item.icon}</ListItemIcon>
                      {item.name}
                    </MenuItem>
                  ))}
              </Menu>
            </>
            <Button
              startIcon={<PersonIcon />}
              component={RouterLink}
              to="/about"
              size="large"
            >
              เกี่ยวกับ
            </Button>
          </div>
          <div className={classes.sectionMobile}>
            <IconButton onClick={handleMobileMenuOpen} color="inherit">
              <MenuIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      <MobileHeader />
    </div>
  );
}
