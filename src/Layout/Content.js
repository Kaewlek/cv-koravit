import React from 'react';
import { makeStyles } from '@material-ui/core';
const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(2),
    marginTop: theme.spacing(10),
  },
}));
export default function Content({ children }) {
  const classes = useStyles();
  return <div className={classes.root}>{children}</div>;
}
