import React, { useContext } from 'react';
import { HeaderContext } from '../Contexts/header.store';
import {
  Box,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  makeStyles,
  Typography,
} from '@material-ui/core';
import { routes } from '../Routes';
import { useHistory } from 'react-router-dom';
import MenuIcon from '@material-ui/icons/Menu';
import PersonIcon from '@material-ui/icons/Person';
const useStyles = makeStyles((theme) => ({
  list: {
    width: '80vw',
    [theme.breakpoints.up('sm')]: {
      width: 300,
    },
  },
}));
export default function MobileHeader() {
  const history = useHistory();
  const classes = useStyles();
  const { mobile, menuSelected } = useContext(HeaderContext);
  const isMobileMenuOpen = Boolean(mobile[0]);
  const handleMobileMenuClose = () => {
    mobile[1](null);
  };
  const handleClickMenu = (index, path) => {
    menuSelected[1](index);
    history.push(path);
    mobile[1](null);
  };
  return (
    <Drawer
      anchor="right"
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <Box display="flex" justifyItems="center">
        <Box mt={1.5} pl={2} flexGrow={1}>
          <Typography variant="h6" color="secondary">
            Menu
          </Typography>
        </Box>
        <Box mt={0.5} pr={2}>
          <IconButton onClick={handleMobileMenuClose}>
            <MenuIcon />
          </IconButton>
        </Box>
      </Box>
      <List className={classes.list}>
        <ListItem
          selected={99 === menuSelected[0]}
          onClick={() => handleClickMenu(99, `/about`)}
          button
        >
          <ListItemIcon>
            <PersonIcon />
          </ListItemIcon>
          <ListItemText primary="About" />
        </ListItem>
        {routes
          .filter(({ isShow }) => {
            return isShow;
          })
          .map((item, index) => (
            <ListItem
              onClick={() => handleClickMenu(index, item.path)}
              button
              key={index}
              selected={index === menuSelected[0]}
            >
              <ListItemIcon>{item.icon}</ListItemIcon>
              <ListItemText primary={item.name} />
            </ListItem>
          ))}
      </List>
    </Drawer>
  );
}
