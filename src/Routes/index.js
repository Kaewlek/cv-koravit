import React from 'react';
import { Switch } from 'react-router-dom';
import { Route } from 'react-router-dom';
import About from '../Pages/About';
import Crud from '../Pages/Crud/Crud';
import ShoppingCart from '../Pages/Shopping/ShoppingCart';
import ProductDetail from '../Pages/Shopping/ProductDetail';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import EditIcon from '@material-ui/icons/Edit';
import PersonIcon from '@material-ui/icons/Person';
import FormatListNumberedIcon from '@material-ui/icons/FormatListNumbered';
import TodoList from '../Pages/TodoList/TodoList';
import InstagramIcon from '@material-ui/icons/Instagram';
import CloneInstagram from '../Pages/CloneInstagram/CloneInstagram';
export default function Routes() {
  return (
    <Switch>
      {routes.map((item, index) => (
        <Route
          key={index}
          exact={item.exact}
          path={item.path}
          component={item.component}
        />
      ))}
      {/* <Route path="*" component={PageNotFound}></Route> */}
    </Switch>
  );
}

export const routes = [
  {
    path: '/',
    name: 'About',
    exact: true,
    component: About,
    isShow: false,
    icon: <PersonIcon />,
  },
  {
    path: '/about',
    name: 'About',
    exact: true,
    component: About,
    isShow: false,
    icon: <PersonIcon />,
  },
  {
    path: '/portfolio/create-read-update-delete',
    name: 'Create Read Update Delete',
    exact: true,
    component: Crud,
    isShow: true,
    icon: <EditIcon />,
  },
  {
    path: '/portfolio/shopping-cart',
    name: 'Shopping Cart',
    exact: true,
    component: ShoppingCart,
    isShow: true,
    icon: <ShoppingCartIcon />,
  },
  {
    path: '/portfolio/shopping-cart/:id',
    name: 'Product Detail',
    exact: true,
    component: ProductDetail,
    isShow: false,
  },
  {
    path: '/portfolio/to-do-list',
    name: 'To Do List',
    exact: true,
    component: TodoList,
    isShow: true,
    icon: <FormatListNumberedIcon />,
  },
  {
    path: '/portfolio/clone-instagram',
    name: 'Clone Instagram',
    exact: true,
    component: CloneInstagram,
    isShow: true,
    icon: <InstagramIcon />,
  },
];
