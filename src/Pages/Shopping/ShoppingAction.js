import React from 'react';
import makeStyles from '@material-ui/styles/makeStyles';
import Badge from '@material-ui/core/Badge';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import FilterListIcon from '@material-ui/icons/FilterList';
import { useDispatch, useSelector } from 'react-redux';
import { handleClickCart } from '../../Reducers/cart.reducer';
import ProductCart from './ProductCart';
const actionStyles = makeStyles((theme) => ({
  root: {
    transform: 'translateZ(0px)',
    flexGrow: 1,
    position: 'fixed',
    right: theme.spacing(2),
    bottom: theme.spacing(2),
  },
  wrapper: {
    marginTop: theme.spacing(3),
  },
  speedDial: {},
}));
export default function ShoppingAction() {
  const dispatch = useDispatch();
  const cartReducer = useSelector((state) => state.cartReducer);
  const classes = actionStyles();
  const [open, setOpen] = React.useState(false);
  const handleClose = () => {
    setOpen(false);
  };
  const handleOpenCart = () => {
    dispatch(handleClickCart());
  };
  const handleOpen = () => {
    setOpen(true);
  };

  return (
    <>
      <div className={classes.root}>
        <div className={classes.wrapper}>
          <SpeedDial
            ariaLabel="Actions"
            className={classes.speedDial}
            icon={<SpeedDialIcon />}
            onClose={handleClose}
            onOpen={handleOpen}
            open={open}
            direction="up"
          >
            <SpeedDialAction
              key={'Cart'}
              icon={
                <Badge color="secondary" badgeContent={cartReducer.data.length}>
                  <ShoppingCartIcon />
                </Badge>
              }
              tooltipTitle={'ตะกร้าสินค้า'}
              onClick={handleOpenCart}
            />
            <SpeedDialAction
              key={'Filter'}
              icon={<FilterListIcon />}
              tooltipTitle={'ปรับแต่งข้อมูลการแสดงผล'}
              onClick={handleClose}
            />
          </SpeedDial>
        </div>
      </div>
      <ProductCart />
    </>
  );
}
