import React, { useContext, useEffect, useState } from 'react';
import {
  Avatar,
  Box,
  Container,
  Grid,
  IconButton,
  makeStyles,
  Typography,
} from '@material-ui/core';
import { useParams } from 'react-router-dom';
import { products } from '../../Mocks/products';
import ShoppingAction from './ShoppingAction';
import { AddShoppingCart } from '@material-ui/icons';
import { AppContext } from '../../Contexts/app.store';
import { useDispatch } from 'react-redux';
import { addProductAsync } from '../../Reducers/cart.reducer';
const useStyles = makeStyles((theme) => ({
  root: {},
  image: {
    height: 'auto',
    maxWidth: '100%',
  },
  container: {
    border: `solid 1px ${theme.palette.divider}`,
  },
  wrapperContent: {
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'column',
  },
  price: {
    color: theme.palette.warning.main,
  },
  detail: {
    textIndent: theme.spacing(4),
  },
  action: {},
  iconAdd: {
    color: theme.palette.success.light,
  },
}));
export default function ProductDetail() {
  const classes = useStyles();
  const { notification } = useContext(AppContext);
  const dispatch = useDispatch();
  const { id } = useParams();
  const [data, setData] = useState({
    id: 0,
    logo: '',
    name: '',
    brand: '',
    image: '',
    detail: '',
    price: 0,
  });

  const addProduct = async () => {
    await dispatch(addProductAsync(data));
    notification('เพิ่มสินค้าสำเร็จ!', 'success');
  };
  useEffect(() => {
    (async () => {
      const result = products.find((f) => f.id === Number(id));
      await setData(result);
    })();
    return () => {};
  }, [id]);
  return (
    <Container className={classes.root}>
      <Grid
        className={classes.container}
        alignContent="center"
        justify="center"
        container
      >
        <Grid xs={12} sm={6} md={5} item>
          <img className={classes.image} src={data?.image} alt="product" />
        </Grid>
        <Grid className={classes.wrapperContent} xs={12} sm={12} md={7} item>
          <div>
            <Box my={1} display="flex">
              <Box flexGrow={1}>
                <Typography component="span" color="textPrimary" variant="h5">
                  {data?.name}
                </Typography>
              </Box>
              <Box>
                <IconButton onClick={() => addProduct()} size="small">
                  <AddShoppingCart className={classes.iconAdd} />
                </IconButton>
              </Box>
            </Box>
            <Box my={1} justifyItems="center" display="flex">
              <Box mr={1}>
                <Avatar src={data?.logo} />
              </Box>
              <Box>
                <Typography
                  gutterBottom
                  component="span"
                  color="textSecondary"
                  variant="subtitle1"
                >
                  {`By ${data?.brand}`}
                </Typography>
              </Box>
            </Box>
            <Typography gutterBottom className={classes.price}>
              {data?.price.toLocaleString('th-TH', {
                style: 'currency',
                currency: 'THB',
              })}
            </Typography>
            <Typography
              className={classes.detail}
              color="textSecondary"
              variant="body1"
            >
              {data?.detail}
            </Typography>
          </div>
          <div className={classes.action}></div>
        </Grid>
      </Grid>
      <ShoppingAction />
    </Container>
  );
}
