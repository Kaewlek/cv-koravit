import React, { useContext } from 'react';
import Avatar from '@material-ui/core/Avatar';
import CardActionArea from '@material-ui/core/CardActionArea';
import Container from '@material-ui/core/Container';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import makeStyles from '@material-ui/styles/makeStyles';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import { grey } from '@material-ui/core/colors';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { Link as RouterLink } from 'react-router-dom';
import { products } from '../../Mocks/products';
import ShoppingAction from './ShoppingAction';
import { addProductAsync } from '../../Reducers/cart.reducer';
import { useDispatch } from 'react-redux';
import { AppContext } from '../../Contexts/app.store';

const cardStyles = makeStyles((theme) => ({
  root: {},
  cardTitle: {
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    display: '-webkit-box',
    WebkitLineClamp: 1,
    WebkitBoxOrient: 'vertical',
  },
  subheader: {
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    display: '-webkit-box',
    WebkitLineClamp: 1,
    WebkitBoxOrient: 'vertical',
  },
  detail: {
    maxHeight: theme.spacing(10),
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    display: '-webkit-box',
    WebkitLineClamp: 3,
    WebkitBoxOrient: 'vertical',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  price: {
    color: theme.palette.warning.light,
  },
  cardAction: {
    position: 'relative',

    '&:hover $view': {
      display: 'block',
    },
  },
  view: {
    display: 'none',
    position: 'absolute',
    margin: 'auto',
    top: '45%',
    left: '45%',
    bottom: 0,
    right: 0,
    color: theme.palette.success.dark,
  },
  iconAdd: {
    color: theme.palette.success.light,
  },
}));
function ProductCard({ item }) {
  const classes = cardStyles();
  const { notification } = useContext(AppContext);
  const dispatch = useDispatch();
  const addProduct = async (data) => {
    await dispatch(addProductAsync(data));
    notification('เพิ่มสินค้าสำเร็จ!', 'success');
  };
  return (
    <>
      <Card className={classes.root}>
        <CardHeader
          avatar={<Avatar src={item.logo} className={classes.avatar}></Avatar>}
          title={item.name}
          subheader={`By ${item.brand}`}
          classes={{
            title: classes.cardTitle,
            subheader: classes.subheader,
          }}
          action={
            <IconButton onClick={() => addProduct(item)}>
              <AddShoppingCartIcon className={classes.iconAdd} />
            </IconButton>
          }
        />
        <CardActionArea
          component={RouterLink}
          to={`/portfolio/shopping-cart/${item.id}`}
          className={classes.cardAction}
        >
          <CardMedia className={classes.media} image={item.image} />
          <CardContent>
            <Typography
              variant="body2"
              color="textSecondary"
              component="p"
              className={classes.detail}
            >
              {item.detail}
            </Typography>
            <Typography className={classes.price} color="textPrimary">
              {item.price.toLocaleString('th-TH', {
                style: 'currency',
                currency: 'THB',
              })}
            </Typography>
          </CardContent>
          <div className={classes.view}>
            <VisibilityIcon color="inherit" fontSize="large" />
          </div>
        </CardActionArea>
      </Card>
    </>
  );
}

const useStyles = makeStyles((theme) => ({
  divider: {
    margin: theme.spacing(1),
  },
  typography: {
    marginLeft: theme.spacing(2),
    color: grey[700],
  },
}));
export default function ShoppingCart() {
  const classes = useStyles();
  return (
    <>
      <Container>
        <Typography className={classes.typography} variant="h5">
          Shopping Cart
        </Typography>
        <Divider className={classes.divider} variant="middle" />
        <Grid container spacing={2}>
          {products.map((item, index) => (
            <Grid key={index} xs={12} sm={6} md={3} lg={3} item>
              <ProductCard item={item} />
            </Grid>
          ))}
        </Grid>
        <ShoppingAction />
      </Container>
    </>
  );
}
