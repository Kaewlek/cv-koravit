import {
  Avatar,
  Box,
  Divider,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText,
  makeStyles,
  TextField,
  Typography,
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import CloseIcon from '@material-ui/icons/Close';
import React, { useContext} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  addProductFromInputAsync,
  deleteProductAsync,
  handleClickCart,
} from '../../Reducers/cart.reducer';
import { ShoppingCart } from '@material-ui/icons';
import { AppContext } from '../../Contexts/app.store';
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  list: {
    width: '100vw',
    [theme.breakpoints.up('sm')]: {
      width: 500,
    },
  },
  price: { color: theme.palette.warning.light },
  count: {
    textAlign: 'right',
    width: theme.spacing(5),
  },
  countDivider: {
    height: theme.spacing(3),
    marginRight: theme.spacing(1),
    marginLeft: theme.spacing(1),
  },
}));
export default function ProductCart() {
  const classes = useStyles();
  const cartReducer = useSelector((state) => state.cartReducer);
  const { notification } = useContext(AppContext);
  const dispatch = useDispatch();
  const handleClose = () => {
    dispatch(handleClickCart());
  };
  const handleChangeCount = (id, count) => {
    dispatch(addProductFromInputAsync({ id, count }));
  };
  const handleDelete = (item) => {
    dispatch(deleteProductAsync(item));
    notification('ลบสินค้าสำเร็จ!', 'info');
  };
  return (
    <Drawer
      anchor="right"
      open={cartReducer.isOpenCart}
      onClose={handleClose}
      classeName={classes.root}
    >
      <Box p={1} display="flex" justifyItems="center">
        <Box mr={1}>
          <ShoppingCart />
        </Box>
        <Box>
          <Typography component="span" variant="subtitle1" color="textPrimary">
            ตะกร้าสินค้า
          </Typography>
        </Box>
        <Box flexGrow={1}></Box>
        <Box>
          <IconButton size="small" onClick={handleClose}>
            <CloseIcon />
          </IconButton>
        </Box>
      </Box>
      <Divider />
      <List className={classes.list}>
        {cartReducer.data.map((item, index) => (
          <ListItem>
            <ListItemAvatar>
              <Avatar src={item.image}></Avatar>
            </ListItemAvatar>
            <ListItemText
              primary={item.name}
              secondary={item.price.toLocaleString('th-TH', {
                style: 'currency',
                currency: 'THB',
              })}
              primaryTypographyProps={{
                variant: 'subtitle1',
                noWrap: true,
              }}
              secondaryTypographyProps={{
                variant: 'body2',
                noWrap: true,
                className: classes.price,
              }}
            />
            <TextField
              label="จำนวน"
              type="number"
              defaultValue={item.count}
              value={item.count}
              className={classes.count}
              onChange={(e) => handleChangeCount(item.id, e.target.value)}
              inputProps={{ min: 1 }}
            />
            <Divider className={classes.countDivider} orientation="vertical" />
            <ListItemSecondaryAction>
              <IconButton
                onClick={() => handleDelete(item)}
                edge="end"
                aria-label="delete"
              >
                <DeleteIcon color="error" />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>
    </Drawer>
  );
}
