import React, { useContext, Fragment } from 'react';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import {
  makeStyles,
  Grid,
  Paper,
  Container,
  TextField,
  Button,
  IconButton,
  Popover,
} from '@material-ui/core';
import { grey } from '@material-ui/core/colors';
import { DatePicker } from 'material-ui-thai-datepickers';
import MUIDataTable from 'mui-datatables';
import moment from 'moment';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers';
import { AppContext } from '../../Contexts/app.store';
import SaveIcon from '@material-ui/icons/Save';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import ModalCrud from './ModalCrud';
import { v4 as uuid } from 'uuid';
const useStyles = makeStyles((theme) => ({
  divider: {
    margin: theme.spacing(1),
  },
  typography: {
    marginLeft: theme.spacing(2),
    color: grey[700],
  },
  form: {
    width: '100%',
    height: theme.spacing(60),
    padding: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    '& > *': {
      marginBottom: theme.spacing(1),
    },
    overflowY: 'auto',
  },
  table: {
    width: '100%',
    height: theme.spacing(60),
    padding: theme.spacing(2),
  },
  submit: {
    color: 'white',
    background: theme.palette.success.light,
    '&:hover': {
      background: theme.palette.success.main,
      color: 'white',
    },
  },
  spring: {
    flex: 1,
  },
}));
export default function Crud() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState({ id: null, target: null });
  const { notification, schemaFormCrud, crud } = useContext(AppContext);
  const { register, handleSubmit, errors, control, setValue } = useForm({
    resolver: yupResolver(schemaFormCrud),
  });
  const columns = [
    {
      name: 'firstName',
      label: 'ชื่อ',
    },
    {
      name: 'lastName',
      label: 'นามสกุล',
    },
    {
      name: 'birthday',
      label: 'วันเกิด',
      options: {
        customBodyRender: (value) => {
          // .add(543, 'year')
          return moment(value).format('DD/MM/YYYY');
        },
      },
    },
    {
      name: 'id',
      label: 'Action',
      options: {
        customBodyRender: (value) => {
          return (
            <Fragment>
              <IconButton onClick={() => handleEdit(value)} size="small">
                <EditIcon color="primary" size="small" />
              </IconButton>
              <IconButton onClick={handleDelete(value)} size="small">
                <DeleteIcon color="error" size="small" />
              </IconButton>
            </Fragment>
          );
        },
      },
    },
  ];
  const handleEdit = async (id) => {
    const data = await crud.data[0].find((f) => f.id === id);
    await crud.edit[1](data);
    await crud.modal[1](true);
  };
  const handleDelete = (id) => (event) => {
    setAnchorEl({ id: id, target: event.currentTarget });
  };
  const onSubmitDelete = (id) => async (event) => {
    const oldData = crud.data[0];
    const newData = await [...oldData].filter((item) => item.id !== id);
    await crud.data[1](newData);
    await setAnchorEl({ id: null, target: null });
    notification('ลบข้อมูลสำเร็จ!', 'info');
  };
  const onSubmit = async (data, e) => {
    const oldData = crud.data[0];
    const newData = [...oldData, { ...data, id: uuid() }];
    await crud.data[1](newData);
    notification('บันทึกข้อมูลสำเร็จ!', 'success');
    e.target.reset();
    setValue('birthday', null);
  };
  return (
    <>
      <Container>
        <Typography className={classes.typography} variant="h5">
          Create Read Update Delete
        </Typography>
        <Divider className={classes.divider} variant="middle" />
        <Grid spacing={1} container>
          <Grid xs={12} md={4} item>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Paper className={classes.form}>
                <TextField
                  label="ชื่อ"
                  variant="outlined"
                  placeholder="กรุณากรอกชื่อ..."
                  fullWidth
                  name="firstName"
                  inputRef={register}
                  helperText={errors.firstName?.message}
                  error={errors.firstName}
                />
                <TextField
                  label="นามสกุล"
                  variant="outlined"
                  placeholder="กรุณากรอกนามสกุล..."
                  fullWidth
                  name="lastName"
                  inputRef={register}
                  helperText={errors.lastName?.message}
                  error={errors.lastName}
                />
                <Controller
                  as={
                    <DatePicker
                      variant="inline"
                      label="วันเกิด"
                      autoOk
                      inputVariant="outlined"
                      fullWidth
                      format="DD/MM/YYYY"
                      inputRef={register}
                      helperText={errors.birthday?.message}
                      error={errors.birthday}
                    />
                  }
                  name="birthday"
                  control={control}
                  defaultValue={null}
                />
                <div className={classes.spring}></div>
                <Button
                  variant="outlined"
                  color="inherit"
                  className={classes.submit}
                  startIcon={<SaveIcon />}
                  type="submit"
                >
                  บันทึก
                </Button>
              </Paper>
            </form>
          </Grid>
          <Grid xs={12} md={8} item>
            <MUIDataTable
              className={classes.table}
              columns={columns}
              options={{
                tableBodyMaxHeight: 350,
                print: false,
                download: false,
                filter: false,
                fixedHeader: true,
                draggableColumns: {
                  enabled: true,
                },
                selectableRows: false,
                elevation: 1,
              }}
              data={crud.data[0]}
            />
          </Grid>
        </Grid>
      </Container>
      <ModalCrud />
      <Popover
        open={Boolean(anchorEl.target)}
        anchorEl={anchorEl.target}
        onClose={() => setAnchorEl({ id: null, target: null })}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        <div>
          <Typography align="center">คุณต้องการลบ?</Typography>
          <Button onClick={() => setAnchorEl({ id: null, target: null })}>
            ยกเลิก
          </Button>
          <Button onClick={onSubmitDelete(anchorEl.id)} color="secondary">
            ตกลง
          </Button>
        </div>
      </Popover>
    </>
  );
}
