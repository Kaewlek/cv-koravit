import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { AppContext } from '../../Contexts/app.store';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers';
import { DatePicker } from 'material-ui-thai-datepickers';
import SaveIcon from '@material-ui/icons/Save';
const useStyles = makeStyles((theme) => ({
  content: {
    '& > *': {
      marginBottom: theme.spacing(1),
    },
  },
  action: {
    marginRight: theme.spacing(2),
  },
  submit: {
    color: 'white',
    background: theme.palette.success.light,
    '&:hover': {
      background: theme.palette.success.main,
      color: 'white',
    },
  },
}));
export default function ModalCrud() {
  const classes = useStyles();
  const { schemaFormCrud, crud, notification } = useContext(AppContext);
  const { register, handleSubmit, errors, control } = useForm({
    resolver: yupResolver(schemaFormCrud),
  });
  const handleClose = () => crud.modal[1](false);
  const onSubmit = async (data, e) => {
    const id = crud.edit[0].id;
    const newEdit = { ...data, id };
    crud.edit[1](newEdit);

    const oldData = crud.data[0];
    const newData = oldData.map((item) => {
      return item.id === newEdit.id ? newEdit : item;
    });
    await crud.data[1](newData);
    e.target.reset();
    handleClose();
    notification('แก้ไขสำเร็จ!', 'success');
  };
  return (
    <Dialog
      open={crud.modal[0]}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">แก้ไขข้อมูล</DialogTitle>
      <form onSubmit={handleSubmit(onSubmit)}>
        <DialogContent className={classes.content}>
          <TextField
            label="ชื่อ"
            variant="outlined"
            placeholder="กรุณากรอกชื่อ..."
            fullWidth
            name="firstName"
            inputRef={register}
            helperText={errors.firstName?.message}
            error={errors.firstName}
            defaultValue={crud.edit[0].firstName}
          />
          <TextField
            label="นามสกุล"
            variant="outlined"
            placeholder="กรุณากรอกนามสกุล..."
            fullWidth
            name="lastName"
            inputRef={register}
            helperText={errors.lastName?.message}
            error={errors.lastName}
            defaultValue={crud.edit[0].lastName}
          />
          <Controller
            as={
              <DatePicker
                variant="inline"
                label="วันเกิด"
                autoOk
                inputVariant="outlined"
                fullWidth
                format="DD/MM/YYYY"
                inputRef={register}
                helperText={errors.birthday?.message}
                error={errors.birthday}
                // yearOffset={543}
              />
            }
            name="birthday"
            control={control}
            defaultValue={crud.edit[0].birthday}
          />
        </DialogContent>
        <DialogActions className={classes.action}>
          <Button onClick={handleClose} color="primary">
            ยกเลิก
          </Button>
          <Button
            variant="outlined"
            color="inherit"
            className={classes.submit}
            startIcon={<SaveIcon />}
            type="submit"
          >
            บันทึก
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
}
