import React, { useState } from 'react';
import {
  Box,
  Card,
  CardContent,
  CardHeader,
  Container,
  Divider,
  Grid,
  IconButton,
  List,
  ListItemText,
  makeStyles,
  Paper,
  TextField,
  Typography,
} from '@material-ui/core';
import { grey } from '@material-ui/core/colors';
import { Add, Check, Close, Delete, Edit, Save } from '@material-ui/icons';
import { v4 as uuid } from 'uuid';
const useStyles = makeStyles((theme) => ({
  divider: {
    margin: theme.spacing(1),
  },
  typography: {
    marginLeft: theme.spacing(2),
    color: grey[700],
  },
  add: {
    color: theme.palette.success.main,
  },
  save: {
    color: theme.palette.success.main,
  },
  contentBoard: {
    height: theme.spacing(50),
    overflowY: 'auto',
  },
  headerWrapper: {
    color: theme.palette.info.main,
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
      height: 'auto',
      marginBottom: theme.spacing(-2),
      '& > *': {
        paddingBottom: theme.spacing(2),
        margin: 'auto',
      },
    },
    [theme.breakpoints.up('sm')]: {
      maxHeight: 64,
    },
  },
  contentWrapper: {
    border: `solid 1px ${theme.palette.divider}`,
    borderRadius: 5,
    padding: theme.spacing(1),
    marginBottom: theme.spacing(1),
    '&:hover $actionButton': {
      display: 'block',
    },
  },
  contentText: {
    wordBreak: 'break-all',
  },
  actionButton: {
    display: 'none',
    [theme.breakpoints.down('xs')]: {
      display: 'block',
    },
  },
}));
export default function TodoList() {
  const classes = useStyles();
  const [text, setText] = useState('');
  const [dataEdit, setDataEdit] = useState({});
  const [data, setData] = useState([
    {
      id: uuid(),
      text:
        'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest',
      isActive: true,
      isEdit: false,
    },
    {
      id: uuid(),
      text: 'testDone',
      isActive: false,
      isEdit: false,
    },
  ]);
  const handleChangeEdit = (e) => {
    setDataEdit({ ...dataEdit, text: e.target.value });
  };
  const handleChange = (e) => {
    setText(e.target.value);
  };
  const handleClickDone = (id) => {
    const result = data.map((item) => {
      return item.id === id ? { ...item, isActive: false } : item;
    });
    setData(result);
  };
  const handleClickEdit = (id) => {
    const result = data.map((item) => {
      if (item.id === id) {
        setDataEdit(item);
        return { ...item, isEdit: true };
      } else {
        return { ...item, isEdit: false };
      }
    });
    setData(result);
  };
  const handleClickDelete = (id) => {
    const result = data.filter((item) => {
      return item.id !== id;
    });
    setData(result);
  };
  const handleSubmitEdit = () => {
    if (!dataEdit.text) {
      return false;
    }
    const result = data.map((item) => {
      if (item.id === dataEdit.id) {
        return { ...item, isEdit: false, text: dataEdit.text };
      } else {
        return { ...item, isEdit: false };
      }
    });
    setData(result);
  };
  const handleCloseEdit = () => {
    const result = data.map((item) => {
      return { ...item, isEdit: false };
    });
    setData(result);
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!text) {
      return false;
    }
    await setData([
      ...data,
      { text, id: uuid(), isActive: true, isEdit: false },
    ]);
    await setText('');
  };
  return (
    <Container>
      <Typography className={classes.typography} variant="h5">
        To Do List
      </Typography>
      <Divider className={classes.divider} variant="middle" />
      <Grid spacing={2} container>
        <Grid xs={12} md={6} item>
          <Card className={classes.paper}>
            <CardHeader
              title="To Do"
              className={classes.headerWrapper}
              titleTypographyProps={{
                noWrap: true,
                variant: 'h6',
              }}
              action={
                <form onSubmit={handleSubmit}>
                  <Box display="flex" alignItems="center">
                    <Box>
                      <TextField
                        value={text}
                        variant="outlined"
                        size="small"
                        placeholder="กรุณากรอกข้อมูล"
                        onChange={handleChange}
                      />
                    </Box>
                    <Box>
                      <IconButton className={classes.add} type="submit">
                        <Add />
                      </IconButton>
                    </Box>
                  </Box>
                </form>
              }
            />
            <Divider />
            <CardContent className={classes.contentBoard}>
              <List>
                {data
                  .filter((item) => item.isActive)
                  .map((item, index) => (
                    <Box
                      alignItems="center"
                      className={classes.contentWrapper}
                      display="flex"
                      key={item.id}
                    >
                      <Box flexGrow={1}>
                        {item.isEdit === false ? (
                          <ListItemText
                            className={classes.contentText}
                            primary={item.text}
                            titleTypographyProps={{
                              noWrap: true,
                            }}
                          />
                        ) : (
                          <TextField
                            value={dataEdit.text}
                            variant="outlined"
                            size="small"
                            placeholder="กรุณากรอกข้อมูล"
                            onChange={handleChangeEdit}
                            fullWidth
                          />
                        )}
                      </Box>
                      {item.isEdit === false ? (
                        <>
                          <Box>
                            <IconButton
                              onClick={() => handleClickDone(item.id)}
                              className={classes.actionButton}
                              size="small"
                            >
                              <Check className={classes.save} />
                            </IconButton>
                          </Box>
                          <Box>
                            <IconButton
                              onClick={() => handleClickEdit(item.id)}
                              className={classes.actionButton}
                              size="small"
                            >
                              <Edit color="primary" />
                            </IconButton>
                          </Box>
                          <Box>
                            <IconButton
                              onClick={() => handleClickDelete(item.id)}
                              className={classes.actionButton}
                              size="small"
                            >
                              <Delete color="error" />
                            </IconButton>
                          </Box>
                        </>
                      ) : (
                        <>
                          <Box ml={1}>
                            <IconButton onClick={handleSubmitEdit} size="small">
                              <Save className={classes.save} />
                            </IconButton>
                          </Box>
                          <Box>
                            <IconButton onClick={handleCloseEdit} size="small">
                              <Close />
                            </IconButton>
                          </Box>
                        </>
                      )}
                    </Box>
                  ))}
              </List>
            </CardContent>
          </Card>
        </Grid>
        <Grid xs={12} md={6} item>
          <Paper className={classes.paper}>
            <CardHeader
              title="Done"
              className={classes.headerWrapper}
              titleTypographyProps={{
                noWrap: true,
                variant: 'h6',
                component: 'span',
                className: classes.save,
              }}
            />
            <Divider />
            <CardContent className={classes.contentBoard}>
              <List>
                {data
                  .filter((item) => !item.isActive)
                  .map((item, index) => (
                    <>
                      <Box
                        alignItems="center"
                        className={classes.contentWrapper}
                        display="flex"
                        key={item.id}
                      >
                        <ListItemText
                          className={classes.contentText}
                          primary={item.text}
                        />
                      </Box>
                    </>
                  ))}
              </List>
            </CardContent>
          </Paper>
        </Grid>
      </Grid>
    </Container>
  );
}
