import React from 'react';
import {
  Box,
  Container,
  Divider,
  makeStyles,
  Typography,
} from '@material-ui/core';
import { grey } from '@material-ui/core/colors';
const useStyles = makeStyles((theme) => ({
  divider: {
    margin: theme.spacing(1),
  },
  typography: {
    marginLeft: theme.spacing(2),
    color: grey[700],
  },
  img: {
    maxWidth: '100%',
    height: 'auto',
    marginLeft: 'auto',
    marginRight: 'auto',
    border: `solid 1px ${theme.palette.divider}`,
  },
}));
export default function CloneInstagram() {
  const classes = useStyles();
  return (
    <Container>
      <Typography className={classes.typography} variant="h5">
        Clone Instagram
      </Typography>
      <Divider className={classes.divider} variant="middle" />
      <Typography className={classes.typography} variant="subtitle2">
        อาจจะไม่เหมือน 100% นะครับ
      </Typography>
      <Typography className={classes.typography} variant="subtitle2">
        Link Web{' '}
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://koravit-clone-instagram.herokuapp.com/"
        >
          https://koravit-clone-instagram.herokuapp.com/
        </a>{' '}
      </Typography>
      <Typography
        gutterBottom
        className={classes.typography}
        variant="subtitle2"
      >
        Git Repository{' '}
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://gitlab.com/Kaewlek/clone-instagram"
        >
          https://gitlab.com/Kaewlek/clone-instagram
        </a>{' '}
      </Typography>
      <Box display="flex">
        <img
          alt="instagram"
          className={classes.img}
          src="../clone-instagram.png"
        />
      </Box>
    </Container>
  );
}
