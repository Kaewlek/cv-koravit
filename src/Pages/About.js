import React, { useContext } from 'react';
import { AppContext } from '../Contexts/app.store';
import Typography from '@material-ui/core/Typography';
import {
  makeStyles,
  Avatar,
  Grid,
  IconButton,
  Tooltip,
} from '@material-ui/core';
import { grey } from '@material-ui/core/colors';
import ProfileContent from '../Components/ProfileContent';
import ProfileTimeline from '../Components/ProfileTimeline';
import ProfileChip from '../Components/ProfileChip';

import PersonIcon from '@material-ui/icons/Person';
import HomeIcon from '@material-ui/icons/Home';
import PhoneIcon from '@material-ui/icons/Phone';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import WorkIcon from '@material-ui/icons/Work';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import LanguageIcon from '@material-ui/icons/Language';
import {
  backends,
  databases,
  educations,
  frontends,
  others,
  positions,
  works,
  gits,
} from '../Mocks/about';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    position: 'relative',
    display: 'block',
  },
  divider: {
    margin: theme.spacing(1),
  },
  typography: {
    marginLeft: theme.spacing(2),
    color: grey[700],
  },
  cover: {
    height: theme.spacing(25),
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    padding: theme.spacing(1),
    [theme.breakpoints.up('md')]: {
      height: theme.spacing(25),
      display: 'block',
    },
    '&:after': {
      content: '""',
      backgroundImage: `url('cover.jpg')`,
      objectFit: 'cover',
      opacity: 0.8,
      backgroundPositionY: 1000,
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      position: 'absolute',
      zIndex: -1,
    },
  },
  profile: {
    display: 'flex',
    width: theme.spacing(15),
    height: theme.spacing(15),
    margin: 'auto',
    [theme.breakpoints.up('md')]: {
      width: theme.spacing(20),
      height: theme.spacing(20),
      position: 'absolute',
      top: theme.spacing(10),
      left: theme.spacing(5),
      '&:hover': {
        cursor: 'pointer',
        opacity: 0.7,
      },
    },
  },
  container: {
    background: grey[200],
    display: 'block',
    paddingTop: theme.spacing(7),
    paddingBottom: theme.spacing(5),
    paddingLeft: theme.spacing(5),
    paddingRight: theme.spacing(5),
  },
  fullName: {
    [theme.breakpoints.up('xs')]: {
      display: 'flex',
      color: 'white',
      margin: 'auto',
    },
    [theme.breakpoints.up('md')]: {
      position: 'absolute',
      top: theme.spacing(17),
      left: theme.spacing(28),
      color: 'white',
      letterSpacing: 1.3,
    },
  },
}));
export default function About() {
  const classes = useStyles();
  const { notification } = useContext(AppContext);
  const onCopy = (value) => {
    navigator.clipboard.writeText(value);
    notification(`คัดลอกเรียบร้อย !`, 'info');
  };
  return (
    <div className={classes.root}>
      <div className={classes.cover}>
        <Avatar src="profile.jpeg" className={classes.profile}></Avatar>
        <Typography variant="h5" className={classes.fullName}>
          กรวิชญ์ แก้วเล็ก
        </Typography>
      </div>
      <div className={classes.container}>
        <Grid container>
          <Grid xs={12} md={6} item>
            <ProfileContent title="ประวัติ" divider titleIcon={<PersonIcon />}>
              ชื่อ นายกรวิชญ์ แก้วเล็ก (โอห์ม) เกิดวันที่ 15 พฤษภาคม พ.ศ. 2541
              อายุ 22 ปี ประสบการณ์ในการทำงาน 1 ปี
            </ProfileContent>
            <ProfileContent title="ที่อยู่" divider titleIcon={<HomeIcon />}>
              บ้านเลขที่ 67/53 หมู่2 หมู่บ้านวโรชา7 ถนนเทพารักษ์-บางเพรียง
              ตำบลบางเพรียง อำเภอบางบ่อ จังหวัดสมุทรปราการ 10560
            </ProfileContent>
            <ProfileContent title="ติดต่อ" divider titleIcon={<PhoneIcon />}>
              <div>
                เบอร์โทรศัพท์ : 087-012-4371
                <Tooltip title="คัดลอกเบอร์โทรศัพท์" placement="right" arrow>
                  <IconButton
                    onClick={(e) => {
                      onCopy('0870124371');
                    }}
                    color="primary"
                    size="small"
                  >
                    <FileCopyIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              </div>
              <div>
                อีเมล : Koravit258@gmail.com
                <Tooltip title="คัดลอกอีเมล" placement="right" arrow>
                  <IconButton
                    onClick={(e) => {
                      onCopy('Koravit258@gmail.com');
                    }}
                    color="primary"
                    size="small"
                  >
                    <FileCopyIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              </div>
              {gits.map((item, index) => (
                <div>
                  {`${item.name} : `}
                  <a target="_blank" rel="noopener noreferrer" href={item.url}>
                    {item.url}
                  </a>
                  <Tooltip
                    title={`คัดลอก ${item.name}`}
                    placement="right"
                    arrow
                  >
                    <IconButton
                      onClick={(e) => {
                        onCopy(item.url);
                      }}
                      color="primary"
                      size="small"
                    >
                      <FileCopyIcon fontSize="small" />
                    </IconButton>
                  </Tooltip>
                </div>
              ))}
            </ProfileContent>
            <ProfileContent
              title="ทักษะ"
              divider
              titleIcon={<LanguageIcon />}
              textIndent={0}
            >
              <div>
                <label>Front end</label>
                <ProfileChip dataSets={frontends} />
              </div>
              <div>
                <label>Back end</label>
                <ProfileChip dataSets={backends} />
              </div>
              <div>
                <label>Database</label>
                <ProfileChip dataSets={databases} />
              </div>
              <div>
                <label>Other</label>
                <ProfileChip dataSets={others} />
              </div>
            </ProfileContent>
            <ProfileContent textIndent={0} title="ตำแหน่งที่สนใจ" divider>
              <ProfileChip dataSets={positions} />
            </ProfileContent>
          </Grid>
          <Grid xs={12} md={6} item>
            <ProfileContent
              title="ประวัติการศึกษา"
              divider
              titleIcon={<MenuBookIcon />}
              textIndent={0}
            >
              <ProfileTimeline dataSets={educations} />
            </ProfileContent>
            <ProfileContent
              title="ประวัติการทำงาน"
              divider
              titleIcon={<WorkIcon />}
              textIndent={0}
            >
              <ProfileTimeline dataSets={works} />
            </ProfileContent>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
