import React from 'react';
import {
  Container,
  Divider,
  makeStyles,
  Paper,
  Typography,
} from '@material-ui/core';
import { grey } from '@material-ui/core/colors';
const useStyles = makeStyles((theme) => ({
  divider: {
    margin: theme.spacing(1),
  },
  typography: {
    marginLeft: theme.spacing(2),
    color: grey[700],
  },
}));
export default function ChatRealTime() {
  const classes = useStyles();
  return (
    <Container>
      <Typography className={classes.typography} variant="h5">
        Chat Realtime
      </Typography>
      <Divider className={classes.divider} variant="middle" />
      <Paper></Paper>
    </Container>
  );
}
