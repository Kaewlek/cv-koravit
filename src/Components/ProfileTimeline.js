import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Timeline from '@material-ui/lab/Timeline';
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineOppositeContent from '@material-ui/lab/TimelineOppositeContent';
import TimelineDot from '@material-ui/lab/TimelineDot';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import { AppContext } from '../Contexts/app.store';
const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(1),
  },
  content: {
    textIndent: theme.spacing(4),
  },
}));
export default function ProfileTimeline({ align = 'alternate', dataSets }) {
  const { breakpoints } = useContext(AppContext);
  const classes = useStyles();
  return (
    <Timeline
      align={align}
    >
      {dataSets &&
        [...dataSets].map((item, index) => (
          <TimelineItem key={index}>
            {item?.time && (
              <TimelineOppositeContent>
                <Typography variant="body2" color="textSecondary">
                  {item.time}
                </Typography>
              </TimelineOppositeContent>
            )}
            <TimelineSeparator>
              <TimelineDot variant="outlined" color="grey">
                {item?.icon && item.icon}
                {item?.imgSrc && breakpoints.md && <Avatar src={item.imgSrc} />}
              </TimelineDot>
              <TimelineConnector />
            </TimelineSeparator>
            <TimelineContent>
              <Paper elevation={3} className={classes.paper}>
                <Typography  align="left" variant="h6" component="h1">
                  {item.title}
                </Typography>
                <Typography
                  className={classes.content}
                  variant="body2"
                  align="left"
                >
                  {item.content}
                </Typography>
              </Paper>
            </TimelineContent>
          </TimelineItem>
        ))}
    </Timeline>
  );
}
