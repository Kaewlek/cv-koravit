import React from 'react';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core';
const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(4),
  },
  divider: {
    margin: theme.spacing(1),
  },
  title: {
    display: 'flex',
    alignItems: 'center',
  },
  paragraph: ({ textIndent }) => ({
    marginLeft: theme.spacing(textIndent),
  }),
  icon: {
    marginRight: theme.spacing(1),
  },
}));
export default function ProfileContent({
  divider,
  titleIcon,
  title,
  children,
  textIndent = 4,
}) {
  const classes = useStyles({ textIndent });
  return (
    <div className={classes.root}>
      <div className={classes.title}>
        <div className={classes.icon}>{titleIcon}</div>
        {title && (
          <Typography component="h3" variant="h6">
            {title}
          </Typography>
        )}
      </div>
      {divider && <Divider className={classes.divider} />}
      <Typography
        paragraph={true}
        className={classes.paragraph}
        variant="body2"
        gutterBottom
        component="span"
      >
        {children}
      </Typography>
    </div>
  );
}
