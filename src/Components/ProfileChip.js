import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(0.5),
    },
  },
}));

export default function ProfileChip({ dataSets }) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      {dataSets &&
        [...dataSets].map((item, index) => (
          <Chip key={index} avatar={item?.imgSrc && <Avatar sizes="small" src={item.imgSrc} />} label={item.label} />
        ))}
    </div>
  );
}
